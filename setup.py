import os
from setuptools import setup

from topdesk_node_exporter import version


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="topdesk_node_exporter",
    version=version.version_string,
    author="Veit Heller",
    author_email="veit@veitheller.de",
    description="Exports various metrics from TOPdesk to Prometheus",
    license="BSD",
    keywords="topdesk node exporter prometheus",
    packages=["topdesk_node_exporter"],
    url="https://gitlab.com/wobcom/topdesk-node-exporter",
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
    install_requires=["prometheus_client", "argparse", "topdesk>=0.0.4"],
    entry_points={
        "console_scripts": ["topdesk-exporter = topdesk_node_exporter.run:run"]
    },
)
