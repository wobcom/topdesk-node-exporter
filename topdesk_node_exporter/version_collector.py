"""
The version collector module.
"""
from prometheus_client.core import GaugeMetricFamily

from topdesk_node_exporter.version import version_string


class VersionCollector:
    """
    The collector for version.
    """

    def collect(self):
        """
        is the collector entry point. It creates a version string value
        """
        gauge = GaugeMetricFamily(
            "topdesk_collector_version", "Version of this collector"
        )
        gauge.add_sample("topdesk_collector_version", {"version": version_string}, 1)
        yield gauge
