"""
Prometheus exporter for exposing various metrics in TOPdesk.
"""
import time
import sys

from prometheus_client import start_http_server
from prometheus_client.core import REGISTRY
import topdesk

from topdesk_node_exporter import (
    change_collector,
    incident_collector,
    version_collector,
    util,
)


def run():
    """
    The main entrypoint for the collector. Sets up the HTTP server and
    collector.
    """
    args = util.parse_args()
    logger = util.get_logger(args)
    tpd = topdesk.Topdesk(
        args.url, verify=(not args.no_verify), app_creds=(args.username, args.token)
    )

    REGISTRY.register(incident_collector.IncidentCollector(tpd, logger))
    REGISTRY.register(change_collector.ChangeCollector(tpd, logger))
    REGISTRY.register(version_collector.VersionCollector())

    start_http_server(args.listen_port, args.listen_address)
    while True:
        time.sleep(1)
