"""
The incident collector module.
"""
from collections import defaultdict
from datetime import datetime, timezone

from prometheus_client.core import GaugeMetricFamily

from topdesk_node_exporter.collector import Collector


class IncidentCollector(Collector):
    """
    The collector for incidents. Groups incidents by operator groups.
    """

    labels = {
        "group": {"name": "operatorGroup", "default": "Assigned to Individual"},
        "operator": {
            "name": "operator",
            "additional": ["status"],
            "default": "Unassigned",
        },
        "type": {"name": "callType", "default": ""},
        "category": {"name": "category", "default": "Uncategorized"},
        "subcategory": {"name": "subcategory", "default": "Uncategorized"},
        "state": {"name": "processingStatus", "default": "No Status"},
    }

    def incidents(self, offset):
        """gets a page of incidents from TOPdesk."""
        return self.tpd.incidents(
            {
                "completed": "false",
                "closed": "false",
                "resolved": "false",
                "page_size": self.page_size,
                "start": offset,
            }
        )

    def major_incidents(self, offset):
        """gets a page of incidents from TOPdesk."""
        return self.tpd.incidents(
            {
                "completed": "false",
                "closed": "false",
                "resolved": "false",
                "major_call": "true",
                "page_size": self.page_size,
                "start": offset,
            }
        )

    def states(self):  # , offset):
        """gets a page of processing states from TOPdesk."""
        return self.tpd.processing_status()

    def parse_date(self, s):
        return datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%f%z")

    def check_date(self, e, now):
        if e.get("targetDate") and self.parse_date(e["targetDate"]) < now:
            return "true", ""
        return "", "true"

    def get_grouped_incidents(self):
        """creates a list of counts of incidents based on labels."""
        d = datetime.now(timezone.utc)
        res = defaultdict(int)

        groups = self.paged(self.operator_groups)

        # initialize all the groups to 0
        for group in groups:
            if group.get("groupName"):
                labels = {k: l["default"] for k, l in self.labels.items()}
                labels["group"] = group["groupName"]
                labels["overdue"] = "true"
                res[tuple(labels.items())] = 0
                labels["overdue"] = ""
                res[tuple(labels.items())] = 0

        # initialize all the states to 0
        for state in self.states():  # self.paged(self.states):
            if state.get("name"):
                labels = {k: l["default"] for k, l in self.labels.items()}
                labels["state"] = state["name"]
                labels["overdue"] = "true"
                res[tuple(labels.items())] = 0
                labels["overdue"] = ""

        for elem in self.paged(self.incidents):
            key = {}
            for k, label in self.labels.items():
                val = elem.get(label["name"])
                key[k] = label["default"]
                if val:
                    key[k] = val["name"]

                # additional fields get added under `key`_`field`
                for field in label.get("additional", []):
                    if val:
                        key["{}_{}".format(k, field)] = val.get(field, "")
            key["overdue"], other = self.check_date(elem, d)
            res[tuple(key.items())] += 1

            # check whether the inverse to overdue exists, if not initialize to 0
            key["overdue"] = other
            if not res.get(tuple(key.items())):
                res[tuple(key.items())] = 0

        return res

    def get_major_incidents(self):
        return len(self.paged(self.major_incidents))

    def collect(self):
        """
        is the collector entry point. It creates gauges based on grouped
        incidents.
        """
        gauge = GaugeMetricFamily("topdesk_incident_count", "Incident Counts")
        for key, count in self.get_grouped_incidents().items():
            gauge.add_sample("topdesk_incident_count", dict(key), count)
        yield gauge

        gauge = GaugeMetricFamily(
            "topdesk_major_incident_count", "Major Incident Count"
        )
        gauge.add_sample("topdesk_major_incident_count", {}, self.get_major_incidents())
        yield gauge
