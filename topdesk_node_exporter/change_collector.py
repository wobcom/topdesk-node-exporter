"""
The change collector module.
"""
from collections import defaultdict
from datetime import date, timedelta

from prometheus_client.core import GaugeMetricFamily

from topdesk_node_exporter.collector import Collector


def get_or(elem, k, dflt):
    """A helper to facilitate chaining in the presence of None in dictionaries"""
    return elem.get(k) or dflt


class ChangeCollector(Collector):
    """
    The collector for today’s changes. Groups change by status and operator
    groups.
    """

    def changes(self):
        """gets a page of today’s changes from TOPdesk."""
        return self.tpd.operator_changes({"fields": "all"})

    def get_grouped_changes(self):
        """creates a list of counts of changes based on state."""
        res = defaultdict(int)

        for elem in self.changes()["results"]:
            status = elem.get("status", {}).get("name") or "Invalid"
            category = elem.get("category", {}).get("name") or "No Category"
            subcategory = elem.get("subcategory", {}).get("name") or "No Subcategory"
            group = (
                elem.get("simple", {}).get("assignee", {}).get("name") or "Not assigned"
            )

            labels = (
                ("group", group),
                ("state", status),
                ("category", category),
                ("subcategory", subcategory),
            )
            res[labels] += 1

        return res

    def collect(self):
        """
        is the collector entry point. It creates gauges based on grouped
        changes.
        """
        gauge = GaugeMetricFamily("topdesk_change_count", "Total Change Count")
        for key, count in self.get_grouped_changes().items():
            gauge.add_sample("topdesk_change_count", dict(key), count)
        yield gauge
