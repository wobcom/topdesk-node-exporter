"""
The base collector module.
"""


class Collector:
    """
    The base collector. Does nothing by default.
    """

    def __init__(self, tpd, logger, page_size=1000):
        self.tpd = tpd
        self.logger = logger
        self.page_size = page_size

    def paged(self, call):
        """paginates a function call."""
        offset = 0
        res = []

        while True:
            page = call(offset)
            if not page:
                break
            res.extend(page)
            offset += len(page)

        return res

    def operator_groups(self, offset):
        """gets a page of operator groups from TOPdesk."""
        return self.tpd.get(
            "/operatorgroups",
            params={
                "page_size": min(self.page_size, 100),
                "start": offset,
            },
        )
