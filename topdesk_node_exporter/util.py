"""
The utility module for the collector. Contains the logger and argparser.
"""
import argparse
import logging
import sys


LOG_FORMAT = "[%(levelname)s] %(message)s"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"


def parse_args(args=None):
    """
    creates and argument parser. Optionally takes arguments as a list instead
    of CLI arguments.
    """
    parser = argparse.ArgumentParser(
        description="TOPdesk node exporter for various TOPdesk metrics"
    )
    parser.add_argument(
        "--log-level",
        action="store",
        type=int,
        help="Log level, see https://docs.python.org/3/library/logging.html#levels",
        default=30,
        dest="log_level",
    )
    parser.add_argument(
        "--listen-port",
        action="store",
        type=int,
        help="Listen port",
        default=8000,
        dest="listen_port",
    )
    parser.add_argument(
        "--listen-address",
        action="store",
        type=str,
        help="Listen address",
        default="0.0.0.0",
        dest="listen_address",
    )
    parser.add_argument(
        "--topdesk-url",
        action="store",
        type=str,
        help="The URL of your TOPdesk instance",
        dest="url",
        required=True,
    )
    parser.add_argument(
        "--topdesk-username",
        action="store",
        type=str,
        help="TOPdesk username",
        dest="username",
        required=True,
    )
    parser.add_argument(
        "--topdesk-token",
        action="store",
        type=str,
        help="TOPdesk token",
        dest="token",
        required=True,
    )
    parser.add_argument(
        "--topdesk-no-verify",
        action="store_true",
        help="Should we skip verifying the certificate of your TOPdesk instance?",
        dest="no_verify",
    )
    return parser.parse_args(args)


def get_logger(args):
    """
    creates a logger based on the arguments.
    """
    logger = logging.getLogger("topdesk-exporter")
    # see https://docs.python.org/3/library/logging.html#levels
    logger.setLevel(args.log_level)

    # Log to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(args.log_level)
    stream_formatter = logging.Formatter(LOG_FORMAT, DATE_FORMAT)
    stream_handler.setFormatter(stream_formatter)

    logger.addHandler(stream_handler)

    return logger
