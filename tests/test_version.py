from topdesk_node_exporter import version_collector, version


def test_collector():
    exporter = version_collector.VersionCollector()
    collected = list(exporter.collect())

    assert len(collected) == 1

    gauge = collected[0]
    assert len(gauge.samples) == 1

    sample = gauge.samples[0]

    assert sample.labels == {"version": version.version_string}
    assert sample.value == 1.0
