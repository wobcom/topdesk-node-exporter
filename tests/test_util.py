from topdesk_node_exporter import util

TEST_ARGS = [
    "--topdesk-username",
    "test",
    "--topdesk-token",
    "test",
    "--topdesk-url",
    "test",
]


def test_arg_parser():
    args = util.parse_args(args=TEST_ARGS)
    assert args.log_level == 30

    args = util.parse_args(args=TEST_ARGS + ["--log-level", "10"])
    assert args.log_level == 10


def test_logger():
    args = util.parse_args(args=TEST_ARGS)

    logger = util.get_logger(args)
    assert logger.isEnabledFor(30)
    assert not logger.isEnabledFor(10)
