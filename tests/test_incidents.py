from topdesk_node_exporter import incident_collector


class MockTopdesk:
    chunk = 3

    mock_data = [
        {
            "operatorGroup": {"name": "group1"},
            "operator": {"name": "test1", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
            "targetDate": "2020-01-01T01:01:01.000+0000",
        },
        {
            "operatorGroup": {"name": "group2"},
            "operator": {"name": "test2", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group3"},
            "operator": {"name": "test3", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group1"},
            "operator": {"name": "test1", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group2"},
            "operator": {"name": "test2", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group3"},
            "operator": {"name": "test3", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group4"},
            "operator": {"name": "test4", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group2"},
            "operator": {"name": "test2", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
        },
        {
            "operatorGroup": {"name": "group2"},
            "operator": {"name": "test2", "status": "operatorGroup"},
            "callType": {"name": "throwaway"},
            "category": {"name": "mycategory"},
        },
        {"operator": {"name": "test6", "status": "operator"}, "callType": None},
    ]

    def __init__(self, call_limit=4):
        self.call_limit = call_limit
        self.called = 0

    def get(self, _, params=None):
        if params and params["start"] != 0:
            return []
        return [
            {"groupName": "test1"},
            {"groupName": "test2"},
            {"groupName": "test3"},
            {"groupName": "test4"},
            {"groupName": "test5"},
        ]

    def processing_status(self):
        return [{"name": "test"}]

    def incidents(self, _):
        if self.called == self.call_limit:
            self.called = 0
            return None

        self.called += 1

        return self.mock_data[(self.called - 1) * self.chunk : self.called * self.chunk]

    def major_incidents(self, _):
        if self.called == self.call_limit:
            self.called = 0
            return None
        return [""]


def test_incidents():
    exporter = incident_collector.IncidentCollector(MockTopdesk(), None)
    incidents = exporter.get_grouped_incidents()

    expected = {
        (
            ("group", "group1"),
            ("operator", "test1"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", "true"),
        ): 1,
        (
            ("group", "group1"),
            ("operator", "test1"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 1,
        (
            ("group", "group2"),
            ("operator", "test2"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 3,
        (
            ("group", "group3"),
            ("operator", "test3"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 2,
        (
            ("group", "group4"),
            ("operator", "test4"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 1,
        (
            ("group", "group2"),
            ("operator", "test2"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "mycategory"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 1,
        (
            ("group", "group5"),
            ("operator", "test5"),
            ("operator_status", "operatorGroup"),
            ("type", "throwaway"),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 0,
        (
            ("group", exporter.labels["group"]["default"]),
            ("operator", "test6"),
            ("operator_status", "operator"),
            ("type", ""),
            ("category", "Uncategorized"),
            ("subcategory", "Uncategorized"),
            ("state", "No Status"),
            ("overdue", ""),
        ): 1,
    }

    for key, data in incidents.items():
        if data != 0:
            assert key in expected
            assert expected[key] == data

    assert 10 == exporter.get_major_incidents()


def test_collector():
    exporter = incident_collector.IncidentCollector(MockTopdesk(), None)
    collected = list(exporter.collect())

    assert len(collected) == 2
