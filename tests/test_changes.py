from topdesk_node_exporter import change_collector


class MockTopdesk:
    chunk = 3

    mock_data = [
        {"status": {"name": "planned"}},
        {"status": {"name": "planned"}, "simple": {"assignee": {"name": "test1"}}},
        {"status": {"name": "in_progress"}},
        {"status": {"name": "in_progress"}},
        {"status": {"name": "in_progress"}},
        {"status": {"name": "in_progress"}, "category": {"name": "Category 2"}},
        {"status": {"name": "in_progress"}, "category": {"name": "Category 1"}, "subcategory": {"name": "Subcategory 1"}},
        {},
        {"status": {}},
    ]

    def get(self, _, params=None):
        if params and params["start"] != 0:
            return []
        return [
            {"groupName": "test1"},
        ]

    def operator_changes(self, *args):
        return {"results": self.mock_data}


def test_changes():
    exporter = change_collector.ChangeCollector(MockTopdesk(), None)
    changes = exporter.get_grouped_changes()

    expected = {
        (
            ("group", "Not assigned"),
            ("state", "planned"),
            ("category", "No Category"),
            ("subcategory", "No Subcategory"),
        ): 1,
        (
            ("group", "test1"),
            ("state", "planned"),
            ("category", "No Category"),
            ("subcategory", "No Subcategory"),
        ): 1,
        (
            ("group", "Not assigned"),
            ("state", "in_progress"),
            ("category", "No Category"),
            ("subcategory", "No Subcategory"),
        ): 3,
        (
            ("group", "Not assigned"),
            ("state", "Invalid"),
            ("category", "No Category"),
            ("subcategory", "No Subcategory"),
        ): 2,
        (
            ("group", "Not assigned"),
            ("state", "in_progress"),
            ("category", "Category 1"),
            ("subcategory", "Subcategory 1"),
        ): 1,
        (
            ("group", "Not assigned"),
            ("state", "in_progress"),
            ("category", "Category 2"),
            ("subcategory", "No Subcategory"),
        ): 1,
    }

    for key, data in changes.items():
        assert key in expected
        assert expected[key] == data


def test_collector():
    exporter = change_collector.ChangeCollector(MockTopdesk(), None)
    collected = list(exporter.collect())

    assert len(collected) == 1
