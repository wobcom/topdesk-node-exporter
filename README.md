# TOPdesk node exporter

Prometheus exporter for exposing various metrics in TOPdesk.

## Installation

To use this exporter, you’ll need Python, `pip`, and `setuptools` on your
system.

Once you have those requirements on your system, you should be able to
either run:

```
pip install -e git+ssh://git@gitlab.service.wobcom.de/infrastructure/topdesk-node-exporter.git
```

Or, if you want to clone the repository instead, you can issue the following
commands:

```
git clone git@gitlab.service.wobcom.de:infrastructure/topdesk-node-exporter.git
cd topdesk-node-exporter
python setup.py install
```

No matter which alternative you choose, you should end up with an executable
called `topdesk-exporter`. If you go for a git-based install, you should do
this in a well-known location, for instance `/opt`, such that updating can
reuse that repository. Otherwise you can safely delete the
`topdesk-node-exporter` directory after installing the service

You can then package it, for instance as a systemd service. An example for a
systemd service file is included [in the examples directory](/examples/systemd).

### Updating

Updating can be done by using `git pull`. If you did a Git clone in `/opt` and
created a systemd service, for instance, updating should look as follows:

```
cd /opt/topdesk-node-exporter/
git pull https://gitlab.service.wobcom.de/infrastructure/topdesk-node-exporter.git
python3 setup.py install
pip3 install -Ur requirements.txt # this is sometimes needed for dependency updates
systemctl restart topdesk-exporter
```

If you used `pip` directly you’ll be able to use fewer steps:

```
pip install -Ue git+ssh://git@gitlab.service.wobcom.de/infrastructure/topdesk-node-exporter.git
systemctl restart topdesk-exporter
```

### Deleting

No matter which installation method you chose, you should be able to delete the
executable from the system using `pip`:

```
pip uninstall topdesk-node-exporter
```

You might be prompted to confirm the deletion action.

If you have a stray repository directory lying around, you can now go ahead and
delete that as well.

## Usage

```
usage: topdesk-exporter [-h] [--log-level LOG_LEVEL]
                        [--listen-port LISTEN_PORT]
                        [--listen-address LISTEN_ADDRESS] --topdesk-url URL
                        --topdesk-username USERNAME --topdesk-token TOKEN
                        [--topdesk-no-verify]

TOPdesk node exporter for various TOPdesk metrics

optional arguments:
  -h, --help            show this help message and exit
  --log-level LOG_LEVEL
                        Log level, see
                        https://docs.python.org/3/library/logging.html#levels
  --listen-port LISTEN_PORT
                        Listen port
  --listen-address LISTEN_ADDRESS
                        Listen address
  --topdesk-url URL     The URL of your TOPdesk instance
  --topdesk-username USERNAME
                        TOPdesk username
  --topdesk-token TOKEN
                        TOPdesk token
  --topdesk-no-verify   Should we skip verifying the certificate of your
                        TOPdesk instance?
```

## Metrics

* Incident count by operator, group, category, subcategory, and type
* Major incident count
* Today’s change count by status and assignee
* Topdesk collector version
