# Changelog

## 0.2.0

- Removed default values for changes
- Added categories and subcategories for changes.

## 0.1.10

- Fix overdue handling for incidents
- Add label for operator group of change
- Refactored CLI runner to work out of the box

## 0.1.9

- Add `topdesk_major_incidents_count` metric

## 0.1.8

- Refactor `operator` and `operatorGroup` handling

## 0.1.7

- Add `topdesk_collector_version` counter

## 0.1.6

- Move from `operatorGroup` property on incidents to `operator` property

## 0.1.5

- Add inverse to selector on whether incidents are overdue (for defaults)
- Add defaults for selector on processing status

## 0.1.4

- Add selector on whether incidents are overdue
- Add selector on processing status

## 0.1.3

- Fix operator groups param size
- Update setup.py to reflect dependencies

## 0.1.2

- Always emit default values for all operator groups in incidents

## 0.1.1

- Added change counters (requires TOPdesk API version >= 1.3.0)

## 0.1.0

- Initial version
